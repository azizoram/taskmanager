package cz.cvut.fel.tasktest.data

data class UserState(
    val userName: String = "username"
)